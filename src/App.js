import logo from "./images/viennastruggle-logo.svg";
import styled from "@emotion/styled";
import Statutes from "./Pages/Statutes";
// import { Chrono } from "react-chrono";

const Container = styled.div`
  /* max-width: 50rem; */
  margin: 2rem;
  border: 2px solid black;
  header {
    position: sticky;
    top: 0;
    border-bottom: 2px solid black;
    background: rgba(255, 255, 0, 0.7);
    img {
      width: calc(100% - 4rem);
      margin: 2rem;
    }
  }
  .ctas {
    line-height: 4rem;
  }
`;

const CTA = styled.a`
  background: #00ff00;
  padding: 1rem;
  border: 2px solid black;
  color: black;
  border-radius: 2rem;
  &:hover {
    background: black;
    color: #ffff00;
  }
`;

const BaseSection = styled.section`
  padding: 2rem;
  img {
    max-width: 100%;
    &.full {
        width: 100%;
      }
  }
`;

const Footer = styled(BaseSection)`
  background: black;
  color: #ffff00;
`;

// const TimelineItemContainer = styled.div``;

// const TimelineItem = ({ title, description, image, video, children }) => {
//   return (
//     <TimelineItemContainer>
//       <div>
//         <h3>{title}</h3>
//         {video && <>Video Url: {video}</>}
//         {image && <img src={image.src} alt={image.caption} />}
//         <p>{description}</p>
//         {children}
//       </div>
//     </TimelineItemContainer>
//   );
// };

function App() {
  return (
    <Container>
      <header>
        <img src={logo} alt="logo" />
      </header>
      <BaseSection>
        <h2>Our Mission</h2>
        <p>Symphonic events exploring digital and analog worlds.</p>
        <div className="ctas">
          <CTA href="https://soundcloud.com/viennastruggle" rel="noreferrer">
            Soundcloud
          </CTA>{" "}
          <CTA href="https://patreon.com/viennastruggle" rel="noreferrer">
            Patreon
          </CTA>{" "}
          <CTA href="https://viennastruggle.bandcamp.com" rel="noreferrer">
            Bandcamp
          </CTA>{" "}
          <CTA href="mailto:mam@viennastruggle.com" rel="noreferrer">
            Need help?
          </CTA>
        </div>
      </BaseSection>
      {/* <BaseSection>
        <h2>History</h2>
        <p>
          We started a little bit before Covid-19 hit us. We wanted to do
          something radically new to keep the bass and the volume and the
          trance, but get a little less chaotic, hedonist and chauvinist. For
          almost two years we operated as an NGO to experiment and define our
          processes revolutionizing the creative process of musical
          collaboration.
        </p>
        <Chrono mode="VERTICAL_ALTERNATING">
          <TimelineItem title="2018 - Vienna Struggle e.V." />
          <TimelineItem
            title="2019 - Club der ersten Stunde"
            description="First creative camp at Robert Karasek Vineyard"
          />
          <TimelineItem title="2019 - Emika Concert in Vienna" />
          <TimelineItem
            title="2020 - Founding of Vienna Struggle Media GmbH"
            description="
          Streaming productions for viennacontemporary, austrian film academy,
          kino der kunst, interface film, wien woche, business riot, period,
          etc."
          />
          <TimelineItem title="2020 - Regular radio shows on res.radio" />
          <TimelineItem title="2020 - Strudle.io goes online" />
          <TimelineItem title="2021 - Album Snaw Crosh is released">
            <a
              href="https://fanlink.to/snawcrosh"
              target="_blank"
              rel="noreferrer"
            >
              Snaw Crosh on spotify, etc.
            </a>{" "}
          </TimelineItem>
          <TimelineItem
            title="2021 - Company split"
            description="Splitting up into Vienna Struggle e.V. and Strudle.io Media
          &amp; Services GmbH"
          />
          <TimelineItem title="2021 - Portal is opened" />
        </Chrono>
        <p>
          This is just the begining of our journey which we want to last for at
          least ten full years. We'd love to see how we can transport our
          colorful history and inspiring past into the next centuries.
        </p>
        <p>
          Stricly following technocratic design processes, there is no
          conductor, no composer, only a holocratic process, circles and roles
          that fluidly self determine their contribution to the Gesamtkunstwerk.
        </p>
      </BaseSection> */}

      <Footer>
        <h2>Location</h2>
        <h3>Portal (XR &amp; Streaming Studio)</h3>
        <p>
          Spittelauer Laende 12/2 / Park &amp; Ride
          <br />
          1090 Wien
          <br />
          Austria
          <br />
          <a href="phone:+436608366059" rel="noreferrer">
            +436608366059
          </a>
        </p>
        <p>
          <CTA
            href="mailto:mandymozart@viennastruggle.com"
            rel="noreferrer"
            target="_blank"
          >
            Reserve a studio visit.
          </CTA>
        </p>
      </Footer>
      <BaseSection>
        <img
          className="full"
          src={
            "https://viennastruggle.at/assets/images/viseve002-layer-art-mobile.jpg"
          }
          alt="Emika in Wien"
        />
        Poster design Tatjana Stürmer
      </BaseSection>
      <Statutes/>
    </Container>
  );
}

export default App;
